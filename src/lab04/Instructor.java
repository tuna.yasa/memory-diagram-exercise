package lab04;

public class Instructor {
    private String name;
    private Course course1;
    private Course course2;

    public Instructor(String name) {
        this.name = name;
    }

    public void setCourse1(Course course1) {
        this.course1 = course1;
    }

    public void setCourse2(Course course2) {
        this.course2 = course2;
    }
}
