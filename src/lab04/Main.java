package lab04;

public class Main {

    public static void main(String[] args) {
        Course cs101 = new Course("CS", 101);
        Course cs102 = new Course("CS", 102);
        Course math112 = new Course("MATH", 112);

        Instructor hasan = new Instructor("Hasan");
        Instructor ismail = new Instructor("Ismail");

        hasan.setCourse1(cs102);
        hasan.setCourse2(cs101);

        ismail.setCourse1(cs101);
        ismail.setCourse2(math112);
    }
}
